from diagrams import Diagram, Cluster
from diagrams.onprem.compute import Server 
from diagrams.onprem.container import Docker
from diagrams.onprem.network import Nginx
from diagrams.onprem.client import User
from diagrams.onprem.database import Postgresql 
from diagrams.onprem.storage import Ceph

from diagrams.generic.database import SQL
from diagrams.generic.storage import Storage
from diagrams.generic.os import LinuxGeneral 

from diagrams.programming.framework import React

with Diagram("Riesling Render Farm", direction="TB") as diag:
    artist = User("Artist")

    with Cluster("Render Farm"):
        with Cluster("Worker Nodes"):
            render_nodes = []
            for _ in range(5):
                render_nodes.append(Server())

        with Cluster("Riesling Render Controller"):
            api_host = Server("Api Server")
            front_end = React("React Frontend")
            with Cluster("Persistence"):
                persistence = [Postgresql("Database"),
                            Storage("File Store"),
                            Ceph("CephFS (Optional)")] 

    artist >> front_end 
    front_end >> api_host
    render_nodes >> api_host 
    api_host >> persistence 

diag
